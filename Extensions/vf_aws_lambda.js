﻿
var aws = require("aws-sdk");
var common = require("./vf_common");
var log = common.log;




var vf_aws_lambda = {

    options: {
        invocationType: 'RequestResponse',
        logEnabled : true,
        batchSize : 100
    },

    invoke: async function (fName, fPayload, message = null) {

        if (message !== null)
            log("vf_aws_lambda.lambda.invoke - " + message);
        else
            log("vf_aws_lambda.lambda.invoke");

        log("vf_aws_lambda.lambda.invoke - fName:" + fName);
        if (vf_aws_lambda.options.logEnabled)
            log("vf_aws_lambda.lambda.invoke - fPayload:" + JSON.stringify(fPayload));



        return new Promise((resolve, reject) => {



            var lambda = new aws.Lambda();

            var params = {
                FunctionName: fName,
                InvocationType: vf_aws_lambda.options.invocationType,
                LogType: 'Tail',
                Payload: JSON.stringify(fPayload)

            };

            if (vf_aws_lambda.options.logEnabledlogEnabled)
                log("vf_aws_lambda.lambda.invoke - params:" + params);

            try {

                lambda.invoke(params, function (err, data) {
                    if (err) {
                        log("vf_aws_lambda.lambda.invoke ERROR");
                        log(err);
                        reject(err);
                    } else {
                        log(data);                     
                        resolve(data.Payload);

                    }
                })

            } catch (e) {
                log("vf_aws_lambda.lambda.invoke ERROR");
                log(e);
                reject(err);
            }

            


        })



    },
    
    invokeBatched: async function (functionName, event, message = null) {

        if (message !== null)
            log("vf_aws_lambda.lambda.invokeBatched - " + message);
        else
            log("vf_aws_lambda.lambda.invokeBatched");




        try {

            var totalPayload = event.Payload;
            log("vf_aws_lambda.lambda.invokeBatched - batchSize: " + totalPayload.length);

            for (var i = 0; i < totalPayload.length; i += vf_aws_lambda.options.batchSize) {
            
                event.Payload = totalPayload.slice(i, i + vf_aws_lambda.options.batchSize);             
                var batchMessage = "Batch: " + i + " - " + (i + vf_aws_lambda.options.batchSize);         
                var promise = await vf_aws_lambda.invoke(functionName, event, batchMessage);              
            }
         
        }
        catch (e) {
            log("vf_aws_lambda.invokeBatched ERROR");
            log(e);            
        }
        



    }


}

module.exports = vf_aws_lambda;