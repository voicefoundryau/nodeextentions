﻿var aws = require("aws-sdk");
var log = require("./vf_common").log;

var vf_transcribe = {

    start: function (record, jobName, outputBucketName, vocabularyName) {

        return new Promise((resolve, reject) => {

            log("vf_transcribe.start");
            log(record);

            const service = new aws.TranscribeService();

            const recordUrl = [
                'https://s3-' + record.AwsRegion + '.amazonaws.com',
                record.S3.Bucket.Name,
                record.S3.Object.Key
            ].join('/');
                      

            var params = {
                LanguageCode: "en-US",
                Media: { MediaFileUri: recordUrl },
                MediaFormat: "wav",
                TranscriptionJobName: jobName,
                MediaSampleRateHertz: 8000,
                OutputBucketName: outputBucketName,
                Settings: { VocabularyName: vocabularyName}
            }

            log("vf_transcribe.start.startTranscriptionJob - params");
            log(params);

            service.startTranscriptionJob(params, function (err, data) {
                if (err) {
                    log(err.stack);
                    reject(err);
                }
                else {
                    log(data);
                    resolve(data);
                }

            });

        });




    },

    getList: function (nameContains, maxResults, status) {

        return new Promise((resolve, reject) => {

            log("vf_transcribe.getList");
          
           
            const service = new aws.TranscribeService();

            


            var params = {
                //JobNameContains: "string",
                //MaxResults: number,
                //NextToken: "string",
                Status: status
            }

            log("vf_transcribe.getList.listTranscriptionJobs - params");
            log(params);

            service.listTranscriptionJobs(params, function (err, data) {
                if (err) {
                    log(err.stack);
                    reject(err);
                }
                else {
                    log(data);
                    resolve(data);
                }

            });

        });




    }

}

module.exports = vf_transcribe;