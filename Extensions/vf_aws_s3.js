﻿
var aws = require("aws-sdk");
var common = require("./vf_common");
var log = common.log;
const service = new aws.S3();




var vf_aws_s3 = {


    getObject: async function (record) {

        log("vf_aws_s3.getObject");
        //setInterval(function () { log("... vf_aws_s3.getObject: " + record.s3.object.key); }, 1000);
      

        return new Promise((resolve, reject) => {

            

            var params = {
                Bucket: record.s3.bucket.name,
                Key: record.s3.object.key,
            };
            log("vf_aws_s3.getObject.params: " + JSON.stringify(params));
            

            service.getObject(params, function (err, data) {
                if (err) {
                    log(err);
                    reject(err);
                }
                else {                    
                    var text = data.Body.toString('utf-8');
                    resolve(text);
                }
            });

        });

    },



}

module.exports = vf_aws_s3;