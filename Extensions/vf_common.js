﻿var aws = require("aws-sdk");
var xml2js = require('xml2js');
var csv = require("csvtojson");

var vf_common = {

    sleep: function (ms) {
        return new Promise(resolve => {
            vf_common.log("sleep:" + ms);
            setTimeout(resolve, ms);

        });
    },

    log : function (message) {

        if (typeof message === 'string' || typeof message === 'number') {
            console.log("== " + message);
        }
        else {
            console.log("== " + JSON.stringify(message));
        }


        //console.log("");
        //console.log("");
    },

    parse: {

        xmlToJson: function (text, logEnabled = true) {
            vf_common.log("vf_common.xmlToJson");
            return new Promise((resolve, reject) => {

                

                xml2js.parseString(text, function (err, result) {
                    if (err) {
                        vf_common.log(err.stack);
                        reject(err);
                    }

                    if (logEnabled) vf_common.log(result);
                    resolve(result);

                });

            });
            

            
        },

        csvToJson: function (text, parseParams, logEnabled = true) {
            vf_common.log("vf_common.csvToJson");
            return new Promise((resolve, reject) => {

                
                
                try {

                    //var result = $.csv.toObjects(text);
                    
                    var result = {};
                    

                    result = csv(parseParams)
                        .fromString(text)
                        .then((jsonObj) => {
                            if (logEnabled) vf_common.log(jsonObj);
                            resolve(jsonObj);
                        });


                   

                }
                catch (e) {
                    vf_common.log(e.stack);
                    reject(e.stack);
                }

            });



        }

    }
   

   
}

module.exports = vf_common;